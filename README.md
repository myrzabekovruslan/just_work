# just_work

Test task.

superuser login: admin

password: admin

## API call.

1. http://127.0.0.1:8000/api/v1/page/ - Список всех страниц, который содержит только их url.

2. http://127.0.0.1:8000/api/v1/page/1 - Страница 1, внутри нее по одному контенту каждого типа. При каждом вызове счетчик контентов увеличивается.
Задача выполняется через Celery.

3. http://127.0.0.1:8000/api/v1/page/2 - Cтраница 2 с пустым контентом.

4. http://127.0.0.1:8000/admin/ - Административная панель.

## Project run

```angular2
docker-compose build
docker-compose up autotests    # запуск тестов
docker-compose up runserver   # запуск проекта
```
