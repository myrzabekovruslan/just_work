from django.db import models
from rest_framework.reverse import reverse


class Page(models.Model):
    title = models.CharField(max_length=255, verbose_name='Заголовок')

    def get_absolute_url(self):
        return reverse('page-detail', args=[str(self.id)])

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-id']
        verbose_name_plural = "Страницы"
        verbose_name = "Страница"


class Video(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название видеофайла')
    page = models.ForeignKey(
        Page,
        related_name="videos",
        on_delete=models.CASCADE,
        verbose_name='Страница',
    )
    file = models.FileField(upload_to='video/%Y/%m/%d/', verbose_name='Видеофайл')
    subtiters = models.FileField(upload_to='video/%Y/%m/%d/', verbose_name='Субтитры к видео')
    counter = models.IntegerField(default=0, verbose_name='Количество просмотров')

    def filename(self):
        print(self.file.name)
        return self.file.name

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Видеофайлы"
        verbose_name = "Видео"


class Audio(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название аудиофайла')
    page = models.ForeignKey(
        Page,
        related_name="audios",
        on_delete=models.CASCADE,
        verbose_name='Страница',
    )
    file = models.FileField(upload_to='audio/%Y/%m/%d/', verbose_name='Аудиофайл')
    bitrate = models.IntegerField(verbose_name='Битрейт в количестве бит в секунду')
    counter = models.IntegerField(default=0, verbose_name='Количество просмотров')

    def filename(self):
        print(self.file.name)
        return self.file.name

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Аудиофайлы"
        verbose_name = "Аудио"


class Text(models.Model):
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    page = models.ForeignKey(
        Page,
        related_name="texts",
        on_delete=models.CASCADE,
        verbose_name='Страница',
    )
    text = models.TextField(verbose_name='Текст')
    counter = models.IntegerField(default=0, verbose_name='Количество просмотров')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Тексты"
        verbose_name = "Текст"