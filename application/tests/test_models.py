from rest_framework.test import APITestCase
from ..models import Page

class PageModelTests(APITestCase):

    def setUp(self):
        self.test_page = Page.objects.create(title="Главная страница")

    def test_object_name_is_title_field(self):
        page = self.test_page
        expected_object_name = "Главная страница"
        self.assertEquals(expected_object_name, str(page))

    def test_get_absolute_url(self):
        page = self.test_page
        self.assertEquals(page.get_absolute_url(), f'/api/v1/page/{page.id}/')