from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase, override_settings
from ..models import Page, Video, Audio, Text
from ..serializers import PageRetrieveSerializer
from django.core.files import File
from django.conf import settings
import os


class PageViewTests(APITestCase):

    def setUp(self):
        self.page = Page.objects.create(
            title="Главная страница",
        )
        with open(os.path.join(settings.BASE_DIR, 'application/tests/files/test_file.wav'), 'rb') as f:
            self.audio = Audio(
                title='audio',
                page=self.page,
                file=File(f, name='audiofile'),
                bitrate=128,
            )
            self.audio.save()
            self.video = Video(
                title='video',
                page=self.page,
                file=File(f, name='videofile'),
                subtiters=File(f,  name='subtiter'),
            )
            self.video.save()
            self.text = Text.objects.create(
                title='text',
                page=self.page,
                text='hello Carl',
            )

    def test_list(self):
        response = self.client.get(reverse('page-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), 1)

    @override_settings(CELERY_TASK_ALWAYS_EAGER=True, CELERY_TASK_EAGER_PROPOGATES=True)
    def test_retrieve(self):
        response = self.client.get(reverse('page-detail', kwargs={'pk': self.page.id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), PageRetrieveSerializer(self.page).data)
        self.assertEqual(self.page.videos.all().first().counter, 1)
        self.assertEqual(self.page.audios.all().first().counter, 1)
        self.assertEqual(self.page.texts.all().first().counter, 1)