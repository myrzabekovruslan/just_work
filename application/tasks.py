from __future__ import absolute_import
from celery import shared_task
from datetime import datetime
import requests
import os
from django.conf import settings
from .models import Page


@shared_task()
def increase_counter(id):
    try:
        audios = Page.objects.get(id=id).audios.all()
        for audio in audios:
            audio.counter += 1
            audio.save()
        videos = Page.objects.get(id=id).videos.all()
        for video in videos:
            video.counter += 1
            video.save()
        texts = Page.objects.get(id=id).texts.all()
        for text in texts:
            text.counter += 1
            text.save()
    except Exception as err:
        print(err)