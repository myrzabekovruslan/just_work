from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import PageViewSet, VideoViewSet, AudioViewSet, TextViewSet


router = DefaultRouter()
router.register('page', PageViewSet, basename='page')
# router.register('video', VideoViewSet, basename='pizza_thickness')
# router.register('cheese_type', AudioViewSet, basename='cheese_type')
# router.register('secret_ingredient', TextViewSet, basename='secret_ingredient')


urlpatterns = [
    path("", include(router.urls)),

]