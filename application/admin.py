from django.contrib import admin
from .models import Page, Video, Audio, Text


class AudioInline(admin.StackedInline):
    model = Audio


class VideoInline(admin.StackedInline):
    model = Video


class TextInline(admin.StackedInline):
    model = Text


class PageAdmin(admin.ModelAdmin):
    search_fields = ['title', ]
    inlines = [AudioInline, VideoInline, TextInline]


class VideoAdmin(admin.ModelAdmin):
    search_fields = ['title']
    autocomplete_fields = ["page"]


class AudioAdmin(admin.ModelAdmin):
    search_fields = ['title']
    autocomplete_fields = ["page"]


class TextAdmin(admin.ModelAdmin):
    search_fields = ['title']
    autocomplete_fields = ["page"]


admin.site.register(Page, PageAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Audio, AudioAdmin)
admin.site.register(Text, TextAdmin)