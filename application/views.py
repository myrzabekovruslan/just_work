from rest_framework import viewsets, permissions, generics, status, serializers, mixins
from .serializers import PageSerializer, VideoSerializer, AudioSerializer, TextSerializer, PageRetrieveSerializer
from .models import Page, Video, Audio, Text
from rest_framework.response import Response
from rest_framework.decorators import action
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .tasks import increase_counter
from datetime import datetime


class PageViewSet(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """Вьюшки для страниц"""
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        increase_counter.delay(instance.id)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return PageRetrieveSerializer
        return self.serializer_class


class VideoViewSet(viewsets.ModelViewSet):
    """Вьюшки для видео"""
    queryset = Video.objects.all()
    serializer_class = VideoSerializer


class AudioViewSet(viewsets.ModelViewSet):
    """Вьюшки для аудио"""
    queryset = Audio.objects.all()
    serializer_class = AudioSerializer


class TextViewSet(viewsets.ModelViewSet):
    """Вьюшки для текстов"""
    queryset = Text.objects.all()
    serializer_class = TextSerializer