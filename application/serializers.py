from rest_framework import serializers
from .models import Page, Video, Audio, Text


class PageSerializer(serializers.ModelSerializer):
    """Сериализатор для страниц"""
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj.get_absolute_url()

    class Meta:
        model = Page
        fields = ("url", )


class VideoSerializer(serializers.ModelSerializer):
    """Сериализатор для видео"""

    class Meta:
        model = Video
        fields = "__all__"


class AudioSerializer(serializers.ModelSerializer):
    """Сериализатор для аудио"""

    class Meta:
        model = Audio
        fields = "__all__"


class TextSerializer(serializers.ModelSerializer):
    """Сериализатор для текста"""

    class Meta:
        model = Text
        fields = "__all__"


class PageRetrieveSerializer(serializers.ModelSerializer):
    """Сериализатор для страницы"""
    videos = VideoSerializer(read_only=True, many=True)
    audios = AudioSerializer(read_only=True, many=True)
    texts = TextSerializer(read_only=True, many=True)

    class Meta:
        model = Page
        fields = "__all__"